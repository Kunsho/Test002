
#include <iostream>
#include <cstdlib>
#include <cstring>
using namespace std;

int main()
{
   int N;
   char symbol;
   bool correct;
   cin>>N;
   if (N<0){ cout<<"An error occured while reading input data"; exit(0);}
   int* A=new int[N];
   for (int i=0;;i++){
   cin>>A[i];
   symbol = cin.get();
   if (symbol == '\n' && i == N-1){
      correct = true;
      break;
      }else if ((symbol == '\n') || (i > N-2)){
        cout<<"An error has occured while reading input data";
        correct = false;
        break;
      }
   }
   if (correct) 
   {
      for (int i = 0; i<(N+1)/2;i++)  swap(A[i],A[N-1-i]);
      for (int i = 0; i<N;i++) cout<<A[i]<<" ";
   }
}
